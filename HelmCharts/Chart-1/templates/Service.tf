apiVersion: v1
kind: Service
metadata:
  name: {{ template "chart.fullname" . }}
  namespace: {{ .Release.Namespace }}
spec:
  selector:
    app: {{ template "chart.fullname" . }}
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  type: NodePort